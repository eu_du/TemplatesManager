import os
import json
import logging
import modules.parser_xls as parser_xls
import modules.parser_xlsx as parser_xlsx


class Source:
    def __init__(self, filename):
        self.__filename = filename
        self.data = self.__parse()

    def __repr__(self):
        return str(self.data)

    def __parse(self):
        datafile_extension = os.path.splitext(self.__filename)[1]
        # определяем тип парсера
        if datafile_extension == '.xls':
            return parser_xls.parse_rule(self.__filename).dataset()
        elif datafile_extension == '.xlsx':
            return parser_xlsx.parse_rule(self.__filename).dataset()
        elif datafile_extension:
            logging.debug('Отсутствует правило обработки источника с расширением ' + datafile_extension)
            return {}
        else:
            return {}

    def dump(self, json_file=None):
        """дампим в json"""
        if json_file is None:
            json_file = os.path.splitext(self.__filename)[0] + '.json'
        json.dump(self.data, open(json_file, 'w'), indent=4, ensure_ascii=False)
