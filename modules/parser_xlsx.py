import openpyxl


class VimpelCom2016:
    def __init__(self, filename):
        self.wb = openpyxl.load_workbook(filename)
        self.ws = self.wb.active
        self.data = {}

    def dataset(self):
        """метод подготавливает и отдает словарь с описанием БС"""
        # ячейки для парсинга 1 (колонка: значение)
        cells_to_parse_1 = ['Номер', 'Наименование', 'Адрес', 'Широта', 'Долгота', 'Доп. информация']
        # ячейки для парсинга 2
        cells_to_parse_2 = ['Радиомодули распределенной БС (RRU)', 'Антенно-фидерное оборудование']

        for row_number in range(1, self.ws.max_row):
            for col_number in range(1, self.ws.max_column):
                current_cell = self.ws.cell(row=row_number, column=col_number)
                if current_cell.value != '' and current_cell.value is not None:
                    if current_cell.value in cells_to_parse_1 and col_number == 1:  # правило для парсинга 1
                        self.data.update({current_cell.value: self.ws.cell(None, row_number, col_number + 2).value})

                    elif current_cell.value in cells_to_parse_2 and col_number == 1:  # правило для парсинга 2
                        if current_cell.value == cells_to_parse_2[0]:  # Радиомодули
                            # имена столбцов: имена в json
                            new_keys = {"Тип RRU": "Тип", "Сектора": "Сектора", "Номер MU": "MU",
                                        "Трансиверы": "Трансиверы"}
                            self.data['Радиомодули'] = []

                            for nrow_index in range(row_number + 2, self.ws.max_row):
                                d = {}
                                # определяем момент остановки парсинга радиомодулей:
                                # если встретится пустая строка или ячейка "Сектора"
                                first_cell_value = self.ws.cell(None, nrow_index, 1).value
                                if first_cell_value == 'Сектора' or first_cell_value == '' or first_cell_value is None:
                                    break
                                counter = 1
                                for new_col_index in range(1, 8):
                                    if self.ws.cell(None, row_number + 1, counter).value in new_keys.keys():
                                        # собираем словарь каждого радиомодуля
                                        d[new_keys[self.ws.cell(None, row_number + 1, counter).value]] =\
                                            self.ws.cell(None, nrow_index, new_col_index).value
                                    counter += 1
                                self.data['Радиомодули'].append(d)
                        elif current_cell.value == cells_to_parse_2[1]:  # Антенны
                            pass

        # TODO удалить ненужное из словаря, добавить остальную инфу в словарь

        # убираю символы переноса в доп.инфо
        self.data['Доп. информация'] = ''.join(self.data.get('Доп. информация').split('\n'))

        # убираю СК-42 представление координат и перемещаю их в секцию Координаты
        self.data['Широта'] = self.data.get('Широта').split()[0]
        self.data['Долгота'] = self.data.get('Долгота').split()[0]
        self.data.update({'Координаты': {'Широта': self.data.get('Широта'), 'Долгота': self.data.get('Долгота')}})

        # удаляю ненужные ключи
        for x in ['Широта', 'Долгота']:
            self.data.pop(x)

        return self.data
#
#     def get_cell_value_by_key(self, key, start_row, start_col, stop_row, stop_col):
#         pass


def parse_rule(filename):
    """Опеределяет какой класс использовать и возвращает его"""
    wb = openpyxl.load_workbook(filename)
    ws = wb.active
    if 'Наименование' in ws['A5'].value:
        return VimpelCom2016(filename)


if __name__ == '__main__':
    ID = VimpelCom2016(r'C:\Users\dukal\YandexDisk\code\Duke\ID_BS_14926_(08.09.2016).xlsx')
    ID.dataset()
