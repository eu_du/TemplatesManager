# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\YandexDisk\code\TemplatesManager\src\widgets\inputdata_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_inputdata_dlg(object):
    def setupUi(self, inputdata_dlg):
        inputdata_dlg.setObjectName("inputdata_dlg")
        inputdata_dlg.resize(419, 103)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(inputdata_dlg.sizePolicy().hasHeightForWidth())
        inputdata_dlg.setSizePolicy(sizePolicy)
        inputdata_dlg.setMinimumSize(QtCore.QSize(0, 0))
        inputdata_dlg.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.verticalLayout = QtWidgets.QVBoxLayout(inputdata_dlg)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(inputdata_dlg)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.path_ldt = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.path_ldt.sizePolicy().hasHeightForWidth())
        self.path_ldt.setSizePolicy(sizePolicy)
        self.path_ldt.setMinimumSize(QtCore.QSize(300, 0))
        self.path_ldt.setObjectName("path_ldt")
        self.horizontalLayout.addWidget(self.path_ldt)
        self.path_btn = QtWidgets.QPushButton(self.groupBox)
        self.path_btn.setObjectName("path_btn")
        self.horizontalLayout.addWidget(self.path_btn)
        self.verticalLayout.addWidget(self.groupBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(inputdata_dlg)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(inputdata_dlg)
        self.buttonBox.accepted.connect(inputdata_dlg.accept)
        self.buttonBox.rejected.connect(inputdata_dlg.reject)
        QtCore.QMetaObject.connectSlotsByName(inputdata_dlg)

    def retranslateUi(self, inputdata_dlg):
        _translate = QtCore.QCoreApplication.translate
        inputdata_dlg.setWindowTitle(_translate("inputdata_dlg", "Исходные данные"))
        self.groupBox.setTitle(_translate("inputdata_dlg", "Путь к файлу"))
        self.path_btn.setText(_translate("inputdata_dlg", "Изменить"))

