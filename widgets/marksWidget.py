import ctypes
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('mycompany.myproduct.subproduct.version')


class marksWidgetClass(QtWidgets.QDialog):
    def __init__(self, context, parent=None):
        super(marksWidgetClass, self).__init__(parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.setModal(QtCore.Qt.NonModal)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowMaximizeButtonHint)
        self.setWindowIcon(QIcon('./icons/template_wizard.ico'))
        self.context = context

        self.mainVerticalLayout = QtWidgets.QVBoxLayout(self)  # (1) создаем главный компоновщик
        self.scrollArea = QtWidgets.QScrollArea(self)  # (2) создаем скролл область
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.Box)
        self.scrollAreaWidgetContents = QtWidgets.QWidget()  # (3) создаем виджет который будет скроллиться
        self.scrollAreaVerticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)  # (4) применяем вертикальный компоновщик к виджету (3)
        self.__fields = self.build_widgets()  # (5)
        self.scrollAreaVerticalLayout.addLayout(self.formLayout)  # (6) добавляем (5) в компоновщик (4)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)  # (7) в (2) устанавливаем (3)
        self.mainVerticalLayout.addWidget(self.scrollArea)  # (8) в (1) добавляем (2)

        # buttonbox
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.mainVerticalLayout.addWidget(self.buttonBox)

        # connects
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.accepted.connect(self.__on_accept)

    def build_widgets(self):
        fields = {}  # при построении формы будет наполняться парами mark:widget
        self.formLayout = QtWidgets.QFormLayout()  # (5) создаем компоновщик формы и заполняем его
        for row, mark in enumerate(self.context):
            if mark != "today":
                label = QtWidgets.QLabel(mark, self.scrollAreaWidgetContents)
                self.formLayout.setWidget(row, QtWidgets.QFormLayout.LabelRole, label)
                line_edit = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
                line_edit.setText(self.context[mark])
                self.formLayout.setWidget(row, QtWidgets.QFormLayout.FieldRole, line_edit)
                fields.update({mark: line_edit})
        return fields

    def __on_accept(self):
        for key in self.__fields.keys():
            # если есть отличия от переданного контекста, то обновляем его
            if self.__fields[key].text() != self.context[key]:
                self.context.update({key: self.__fields[key].text()})
