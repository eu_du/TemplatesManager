Зависимости
------------

* [Python 3.6.0](https://www.python.org/downloads)
* [PyQt5](https://pypi.python.org/pypi/PyQt5)
* [docxtpl](https://pypi.python.org/pypi/docxtpl)
* [openpyxl](https://pypi.python.org/pypi/openpyxl) 
* [xlrd](https://pypi.python.org/pypi/xlrd)
