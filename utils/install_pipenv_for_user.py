import sys
import site
import os
import subprocess

subprocess.run(["pip", "install", "--user", "pipenv"])

print("Your OS is: " + sys.platform)
if sys.platform == 'win32':
    scriptsPath = os.path.join(os.path.dirname(site.USER_SITE), "Scripts")
    tail = os.path.relpath(scriptsPath, os.path.expanduser("~"))
    subprocess.run(["python", "path_variables.py", os.path.join("%USERPROFILE%", tail)])
