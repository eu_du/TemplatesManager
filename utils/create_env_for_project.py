"""
This module needs installed pipenv and "Script" 
path in user variables for win32 OS
"""
import subprocess
import os

path = os.path.abspath('./../')
os.chdir(path)
print("Current directory changed to: " + path)
subprocess.run(["pipenv", "install", "--dev"])
input("Press Enter to continue...")
