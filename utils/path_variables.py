import winreg
import sys
import subprocess

REG_PATH = r"Environment"
def get_reg(name):
    try:
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0,
                                       winreg.KEY_READ)
        value, regtype = winreg.QueryValueEx(registry_key, name)
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None

def set_reg(name, value):
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, REG_PATH)
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0, 
                                       winreg.KEY_WRITE)
        winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False

def add_value(value):
    OLDPATH = get_reg("path")
    if value not in OLDPATH:
        NEWPATH = OLDPATH + value
        subprocess.run(["setx", "PATH", NEWPATH + ";"])


if len(sys.argv) > 1:
    add_value(sys.argv[1])
