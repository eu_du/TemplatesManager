# CALL %CD%\env\Lib\site-packages\pyqt5-tools\designer.exe
import subprocess
import os
import sys
import io

path = os.path.abspath('./../')
os.chdir(path)
print("Current directory changed to: " + path)
proc = subprocess.Popen(["pipenv", "--venv"], stdout=subprocess.PIPE)
for line in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
    designer_path = os.path.join(line.strip(), "Lib\site-packages\pyqt5-tools\designer.exe")
    print(f"Running: {designer_path}")
    subprocess.Popen(designer_path)
